package com.company;

import java.util.*;

class Deck {
    private static final Set<Card> ALL_CARDS = getAllCards(); // все карты колоды

    private static Set<Card> getAllCards() {
        Set<Card> allCards = new HashSet<>();
        for (Rank rank : Rank.values()) {
            for (Suit suit : Suit.values()) {
                allCards.add(new Card(rank, suit));
            }
        }
        return Collections.unmodifiableSet(allCards);
    }

    private final Deque<Card> cards = new ArrayDeque<>();

    public Deck() {
        // копируем во временный список, перемешиваем и копируем в стек
        List<Card> temp = new ArrayList<>(ALL_CARDS);
        Collections.shuffle(temp);
        cards.addAll(temp);
    }

    public void shuffle() {
        List<Card> temp = new ArrayList<>();
        while (!cards.isEmpty()) {
            temp.add(cards.pop());
        }
        Collections.shuffle(temp);
        cards.addAll(temp);
    }

    // Добавление вниз колоды
    public void reinsert(Card card) {
        cards.addLast(card);
    }

    public Card draw() {
        return cards.pop();
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }
}
