package com.company;

enum Rank {
    RANK_6,
    RANK_7,
    RANK_8,
    RANK_9,
    RANK_10,
    JACK,
    QUEEN,
    KING,
    ACE
}

