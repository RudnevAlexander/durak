package com.company;


import java.util.List;
import java.util.Scanner;

public class Durak {

    public static void main(String[] args) {
        int playersCount = readPlayersCount();

        Hand[] hands = new Hand[playersCount];
        for (int i = 0; i < playersCount; i++) {
            hands[i] = new Hand();
        }

        Deck deck = new Deck();

        Card trump = null;
        if (playersCount == 6) { // если игроков 6, то берем карту сверху колоды, возвращаем её назад и перемешиваем колоду
            trump = deck.draw();
            deck.reinsert(trump);
            deck.shuffle();
        }

        for (int i = 0; i < 6; i++) { // раздаём по 6 карт
            for (int player = 0; player < playersCount; player++) {
                hands[player].addCard(deck.draw());
            }
        }

        if (playersCount != 6) {  // если игроков меньше 6, то берем следующую карту из колоды, возвращаем её вниз
            trump = deck.draw();
            deck.reinsert(trump);
        }

        System.out.println("Козырь: " + formatCard(trump));

        int maxPower = 0;
        int maxPlayerPower = 0;
        for (int i = 0; i < hands.length; i++) {
            Hand hand = hands[i];
            int playerNumber = i + 1;
            System.out.println("Карты игрока " + playerNumber + ": " + formatHand(hand));

            int power = getPower(hand, trump.getSuit());
            if (power > maxPower) {
                maxPower = power;
                maxPlayerPower = playerNumber;
            }
        }

        System.out.println("Лучший набор карт у игрока " + maxPlayerPower);
    }

    private static int readPlayersCount() {
        System.out.println("\nВведите количество игроков...\n");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                int players = Integer.parseInt(scanner.nextLine());
                if (players < 2 || players > 6) {
                    throw new IllegalArgumentException();
                }
                return players;
            } catch (Exception e) {
                System.out.println("Введите число от 2 до 6");
            }
        }
    }

    private static String formatHand(Hand hand) {
        StringBuilder sb = new StringBuilder();
        List<Card> cards = hand.getCards();
        for (Card card : cards) {
            sb.append(formatCard(card));
        }
        return sb.toString();
    }

    private static String formatCard(Card card) {
        String rank = formatRank(card.getRank());
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(rank);
        if (rank.length() == 1) {
            sb.append(" ");
        }
        sb.append(formatSuit(card.getSuit())).append("]");
        return sb.toString();
    }

    private static String formatRank(Rank rank) {
        switch (rank) {
            case RANK_6:
                return "6";
            case RANK_7:
                return "7";
            case RANK_8:
                return "8";
            case RANK_9:
                return "9";
            case RANK_10:
                return "10";
            case JACK:
                return "J";
            case QUEEN:
                return "Q";
            case KING:
                return "K";
            case ACE:
                return "A";
        }
        throw new RuntimeException();
    }

    private static String formatSuit(Suit suit) {
        switch (suit) {
            case HEARTS:
                return "♥";
            case DIAMONDS:
                return "♦";
            case CLUBS:
                return "♣";
            case SPADES:
                return "♠";
        }
        throw new RuntimeException();
    }

    private static int getPower(Hand hand, Suit trump) {
        int power = 0;
        for (Card card : hand.getCards()) {
            power += getPower(card, trump);
        }
        return power;
    }

    private static int getPower(Card card, Suit trump) {
        int power;
        switch (card.getRank()) {
            case RANK_6:
                power = 1;
                break;
            case RANK_7:
                power = 2;
                break;
            case RANK_8:
                power = 3;
                break;
            case RANK_9:
                power = 4;
                break;
            case RANK_10:
                power = 5;
                break;
            case JACK:
                power = 6;
                break;
            case QUEEN:
                power = 7;
                break;
            case KING:
                power = 8;
                break;
            case ACE:
                power = 9;
                break;
            default:
                throw new RuntimeException();
        }

        if (card.getSuit() == trump) {
            power += 9;
        }

        return power;
    }
}