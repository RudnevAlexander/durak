package com.company;

class Card {
    private final Rank rank;
    private final Suit suit;

    // Создание карты с указанным рангом и мастью
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return "Card{rank=" + rank + ", suit=" + suit + '}';
    }
}
