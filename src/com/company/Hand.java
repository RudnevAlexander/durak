package com.company;

import java.util.ArrayList;
import java.util.List;

class Hand {
    private final List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        return "Hand{cards=" + cards + '}';
    }
}
